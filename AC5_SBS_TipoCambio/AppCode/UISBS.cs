﻿using AC5_SBS_TipoCambio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AC5_SBS_TipoCambio.AppCode
{
    public partial class UI
    {
        public class SBS
        {
            public static async Task<ResultadoEN> TipoDeCambio(string fecha_inicio, string fecha_fin = "")
            {
                ResultadoEN resultado = new ResultadoEN();
                try
                {
                    if (string.IsNullOrEmpty(fecha_fin) | string.IsNullOrWhiteSpace(fecha_fin))
                        fecha_fin = fecha_inicio;
                    var URL = $"http://www.sbs.gob.pe/app/stats/seriesH-tipo_cambio_moneda_excel.asp?fecha1={fecha_inicio}&fecha2={fecha_fin}&moneda=02";
                    System.Net.CookieContainer cookies = new System.Net.CookieContainer();
                    var DATA = "";
                    HttpClient client = new HttpClient(new HttpClientHandler() { CookieContainer = cookies, UseCookies = true, AllowAutoRedirect = true });
                    HttpResponseMessage response = await client.GetAsync(URL);
                    if (response.IsSuccessStatusCode)
                    {
                        DATA = await response.Content.ReadAsStringAsync();
                        DATA = DATA.Replace("�", "ó");
                        resultado = UI.__Regex.Create(DATA);
                    }
                }
                catch (Exception ex)
                {
                    resultado.mensaje = ex.Message;
                }
                return resultado;
            }
        }
    }

}
