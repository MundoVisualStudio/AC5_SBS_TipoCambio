﻿using System;

namespace AC5_SBS_TipoCambio.AppCode
{
	public partial class UI
	{
		public static string Value(object Valor, string DefaultValue = "")
		{
			string NuevoValor;
			try
			{
				NuevoValor = Convert.ToString(Valor);
				if (string.IsNullOrEmpty(NuevoValor) || Valor == null || Valor == DBNull.Value)
					NuevoValor = DefaultValue;
			}
			catch (Exception)
			{
				NuevoValor = DefaultValue;
			}

			return NuevoValor;
		}

		public static string ValueTrim(object Valor, string DefaultValue = "")
		{
			string NuevoValor = UI.Value(Valor, DefaultValue);
			NuevoValor = NuevoValor.Trim();
			return NuevoValor;
		}
		public static string ValueTrimFull(string node, bool cleartags = false)
		{
			if (cleartags)
			{
				node = UI.__Regex.Trim(node);
			}
			node = UI.ValueTrim(node);
			node = node.Replace(Environment.NewLine, "");
			node = node.Replace("\t", "");
			node = node.Replace(new string(' ', 10), " ");
			node = node.Replace(new string(' ', 9), " ");
			node = node.Replace(new string(' ', 8), " ");
			node = node.Replace(new string(' ', 7), " ");
			node = node.Replace(new string(' ', 6), " ");
			node = node.Replace(new string(' ', 5), " ");
			node = node.Replace(new string(' ', 4), " ");
			node = node.Replace(new string(' ', 3), " ");
			node = node.Replace(new string(' ', 2), " ");
			return node;
		}
	}
}