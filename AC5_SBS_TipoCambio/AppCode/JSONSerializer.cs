﻿using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace AC5_SBS_TipoCambio.AppCode
{
	public class JSONSerializer<T> where T : class, new()
	{
		public static string Serialize(T instance)
		{
			var serializer = new DataContractJsonSerializer(typeof(T));
			using (var stream = new MemoryStream())
			{
				serializer.WriteObject(stream, instance);
				return Encoding.UTF8.GetString(stream.ToArray());
			}
		}
	}
}
