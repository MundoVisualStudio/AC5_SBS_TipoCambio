﻿using AC5_SBS_TipoCambio.Models;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AC5_SBS_TipoCambio.AppCode
{
	public partial class UI
	{
		public class __Regex
		{
			private const RegexOptions ExpressionOptions = RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase;

			public static string Trim(string This)
			{
				Regex tagRemove = new Regex("<[^>]*(>|$)");
				string result = tagRemove.Replace(This, string.Empty);
				return result;
			}
			public static string GetTag(string This, string Tag)
			{
				Regex regex = new Regex($@"<\s*{Tag}[^>]*>(.*?)<\s*/\s*{Tag}>", ExpressionOptions);
				System.Text.StringBuilder results = new System.Text.StringBuilder();
				foreach (Match m in regex.Matches(This))
					results.Append(m.Value);
				return results.ToString();
			}
			public static List<string> GetTags(string This, string Tag)
			{
				var CleanThis = UI.__Regex.GetTag(This, Tag);
				List<string> results = new List<string>();
				Regex _reg = new Regex($@"<{Tag}>(?<data>[\s\S]*?)<\/{Tag}>", ExpressionOptions);
				MatchCollection doregex2 = _reg.Matches(CleanThis);
				foreach (Match item in doregex2)
					results.Add(item.Value);
				return results;
			}

			public static ResultadoEN Create(string HTML)
			{
				var _HTML = UI.ValueTrimFull(HTML);
				ResultadoEN Resultado = new ResultadoEN();
				try
				{
					var lstTRs = UI.__Regex.GetTags(_HTML, "tr");
					if (lstTRs != null && lstTRs.Count > 1)
					{
						for (var i = 1; i <= lstTRs.Count - 1; i++)
						{
							var item = lstTRs[i];
							var lstTDs = UI.__Regex.GetTags(item, "td");
							TipoCambioEN objTemp = new TipoCambioEN();
							objTemp.fecha = UI.ValueTrimFull(lstTDs[0], true);
							objTemp.moneda_descripcion = UI.ValueTrimFull(lstTDs[1], true);
							objTemp.precio_compra = UI.ValueTrimFull(lstTDs[2], true);
							objTemp.precio_venta = UI.ValueTrimFull(lstTDs[3], true);
							Resultado.tipo_cambio.Add(objTemp);
						}
						Resultado.success = true;
					}
				}
				catch (Exception ex)
				{
					Resultado.mensaje = ex.Message;
				}
				return Resultado;
			}
		}
	}
}