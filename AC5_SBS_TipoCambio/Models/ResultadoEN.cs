﻿using AC5_SBS_TipoCambio.AppCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AC5_SBS_TipoCambio.Models
{
    public class ResultadoEN
    {
        public bool success { get; set; } = false;
        public string mensaje { get; set; } = "";
        public List<TipoCambioEN> tipo_cambio { get; set; } = new List<TipoCambioEN>();
        public string AsJson()
        {
            string resultado;
            try
            {
                resultado = JSONSerializer<ResultadoEN>.Serialize(this);
            }
            catch (Exception ex)
            {
                resultado = ex.Message;
            }
            return resultado;
        }
    }
}
