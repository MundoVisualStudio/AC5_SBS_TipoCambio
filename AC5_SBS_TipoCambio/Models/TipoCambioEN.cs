﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AC5_SBS_TipoCambio.Models
{
    public class TipoCambioEN
    {
        public string fecha { get; set; }
        public string moneda_descripcion { get; set; }
        public string precio_compra { get; set; }
        public string precio_venta { get; set; }
    }
}
